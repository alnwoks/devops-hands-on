# Jenkins Pipeline for Containerized Nodejs web app

This repo contains files used to demonstrate a simple Jenkins Pipeline script for containerizing and deploying a NodeJS web application as part of the requirements for the Technical assessment part of the DevOps Engineer position at RenMoney.

## Jenkins Endpoint

To access the Jenkins server, use the details below:
```bash
URL: https://jenkins.clustr.ml
username: renmoney
password: P@ssw0rd123
```
## Deployed application

You can confirm the node applications using the link below:
[NodeJS-App](https://www.clustr.ml/)

## JenkinsFile repo
The jenkinsfile can be accessed in this repo:

I hope this is satisfactory!
